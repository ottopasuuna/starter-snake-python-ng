import random

DIRECTIONS = ['up', 'down', 'left', 'right']

class SnakeAI:

    def __init__(self):
        self.game_instance = None

    def __call__(self, game_state):
        '''Top level snake ai call'''
        return self.move_random(game_state)


    def move_random(self, game_state):
        return random.choice(DIRECTIONS)
