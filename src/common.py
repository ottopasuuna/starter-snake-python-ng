import logging

def getLogger(name):
    formatter = logging.Formatter('%(asctime)s| %(name)-10s| %(levelname)-8s| %(message)s')
    handler = logging.StreamHandler()
    handler.setFormatter(formatter)
    handler.setLevel(logging.DEBUG)

    log = logging.getLogger(name)
    log.addHandler(handler)
    log.setLevel(logging.DEBUG)
    return log

DIRECTIONS = ['up', 'down', 'left', 'right']

direction_to_vector = {
    'up': (0, -1),
    'down': (0, 1),
    'left': (-1, 0),
    'right': (1, 0)
}

vector_to_direction = {
    (0, -1): 'up',
    (0, 1): 'down',
    (-1, 0): 'left',
    (1, 0): 'right'
}

def square_for_direction(square, direction):
    x, y = square
    dx, dy = direction_to_vector[direction]
    return (x + dx, y + dy)

def direction_for_squares(sq1, sq2):
    dx = sq2[0] - sq1[0]
    dy = sq2[1] - sq1[1]
    assert dx in (0, 1, -1)
    assert dy in (0, 1, -1)
    return vector_to_direction[(dx, dy)]

