import unittest
from gameobjects import GameState, Board, Snake


class TestSnake(unittest.TestCase):

    @staticmethod
    def make_snake():
        return Snake('abc123', 'Snake', 100, [(1, 2), (1, 3)])

    @staticmethod
    def make_dict():
        d = {
            'id': 'abc123',
            'name': 'Snake',
            'health': 100,
            'body': [{'x':1, 'y':2}, {'x':1, 'y':3}]
        }
        return d

    def test_from_dict(self):
        d = TestSnake.make_dict()

        expected_snake = TestSnake.make_snake()
        new_snake = Snake.from_dict(d)
        self.assertEqual(new_snake, expected_snake)

    def test_to_dict(self):
        snake = TestSnake.make_snake()
        expected_dict = TestSnake.make_dict()
        new_dict = snake.to_dict()
        self.assertEqual(new_dict, expected_dict)

class TestBoard(unittest.TestCase):

    @staticmethod
    def make_board():
        return Board(11, 11, [(4, 5)], [TestSnake.make_snake()])

    @staticmethod
    def make_dict():
        d = {
            'height': 11,
            'width': 11,
            'food': [{'x':4, 'y':5}],
            'snakes': [TestSnake.make_dict()]
        }
        return d

    def test_from_dict(self):
        d = TestBoard.make_dict()
        expected_board = TestBoard.make_board()
        new_board = Board.from_dict(d)
        self.assertEqual(new_board, expected_board)

    def test_to_dict(self):
        board = TestBoard.make_board()
        expected_dict = TestBoard.make_dict()
        new_dict = board.to_dict()
        self.assertEqual(new_dict, expected_dict)


class TestGameState(unittest.TestCase):

    @staticmethod
    def make_game():
        return GameState('abc123', 1, TestBoard.make_board(), TestSnake.make_snake())

    @staticmethod
    def make_dict():
        d = {
            'game': { 'id': 'abc123'},
            'turn': 1,
            'board': TestBoard.make_dict(),
            'you': TestSnake.make_dict()
        }
        return d

    def test_from_dict(self):
        d = TestGameState.make_dict()
        expected_board = TestGameState.make_game()
        new_board = GameState.from_dict(d)
        self.assertEqual(new_board, expected_board)

    def test_to_dict(self):
        board = TestGameState.make_game()
        expected_dict = TestGameState.make_dict()
        new_dict = board.to_dict()
        self.assertEqual(new_dict, expected_dict)
