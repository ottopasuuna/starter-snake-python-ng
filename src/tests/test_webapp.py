import unittest
from webtest import TestApp
from main import application
from tests.test_gameobjects import TestGameState
from api import ALLOWED_HEAD_TYPES, ALLOWED_TAIL_TYPES, ALLOWED_MOVEMENTS


class TestWebApp(unittest.TestCase):

    def setUp(self):
        self.app = TestApp(application)

    def test_root(self):
        resp = self.app.get('/')
        self.assertEqual(resp.status, '200 OK')

    def test_start(self):
        game_state = TestGameState.make_dict()
        resp = self.app.post_json('/start', game_state)
        self.assertEqual(resp.status, '200 OK')
        allowed_start_responses = {'color', 'headType', 'tailType'}
        for key, value in resp.json.items():
            self.assertIn(key, allowed_start_responses)
            if key == 'color':
                self.assertIsInstance(value, str)
            elif key == 'headType':
                self.assertIn(value, ALLOWED_HEAD_TYPES)
            elif key == 'tailType':
                self.assertIn(value, ALLOWED_TAIL_TYPES)

    def test_end(self):
        game_state = TestGameState.make_dict()
        resp = self.app.post_json('/end', game_state)
        self.assertEqual(resp.status, '200 OK')

    def test_move(self):
        game_state = TestGameState.make_dict()
        resp = self.app.post_json('/move', game_state)
        self.assertEqual(resp.status, '200 OK')
        self.assertIn(resp.json['move'], ALLOWED_MOVEMENTS)
