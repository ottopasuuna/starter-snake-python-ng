import unittest
from simulator import next_game_state
from gameobjects import GameState, Board, Snake


class TestSimulator(unittest.TestCase):


    def test_single_snake_move(self):
        snake = Snake(1, 'test', 100, [(5, 5), (5, 6), (5, 7)])
        board = Board(11, 11, [], [snake])
        initial_state = GameState(1, 1, board, snake)
        snake_moves = {1: 'up'}
        next_state = next_game_state(initial_state, snake_moves)
        self.assertEqual(next_state.you.body, [(5, 4), (5, 5), (5, 6)])
        self.assertEqual(next_state.you.health, 99)
        self.assertEqual(next_state.turn, 2)
        # Original state must not be altered
        self.assertEqual(snake.body, [(5, 5), (5, 6), (5, 7)])
        self.assertNotEqual(initial_state, next_state)

    def test_starved(self):
        snake = Snake(1, 'test', 1, [(5, 5), (5, 6), (5, 7)])
        board = Board(11, 11, [], [snake])
        initial_state = GameState(1, 1, board, snake)
        snake_moves = {1: 'up'}
        next_state = next_game_state(initial_state, snake_moves)
        self.assertEqual(next_state.board.snakes, [])
        # Original state must not be altered
        self.assertEqual(snake.body, [(5, 5), (5, 6), (5, 7)])
        self.assertNotEqual(initial_state, next_state)

    def test_eat_food(self):
        snake = Snake(1, 'test', 1, [(5, 5), (5, 6), (5, 7)])
        board = Board(11, 11, [(5, 4)], [snake])
        initial_state = GameState(1, 1, board, snake)
        snake_moves = {1: 'up'}
        next_state = next_game_state(initial_state, snake_moves)
        self.assertEqual(next_state.you.health, 100)
        self.assertEqual(next_state.board.snakes, [next_state.you])
        self.assertEqual(next_state.board.food, [])
        # Original state must not be altered
        self.assertEqual(snake.body, [(5, 5), (5, 6), (5, 7)])
        self.assertNotEqual(initial_state, next_state)

    def test_out_of_bounds(self):
        snake = Snake(1, 'test', 100, [(5, 0), (5, 1), (5, 2)])
        board = Board(11, 11, [], [snake])
        initial_state = GameState(1, 1, board, snake)
        snake_moves = {1: 'up'}
        next_state = next_game_state(initial_state, snake_moves)
        self.assertEqual(next_state.board.snakes, [])
        # Original state must not be altered
        self.assertEqual(snake.body, [(5, 0), (5, 1), (5, 2)])
        self.assertNotEqual(initial_state, next_state)

    def test_head_collision(self):
        snake1 = Snake(1, 'test1',  100, [(5, 5), (5, 6), (5, 7)])
        snake2 = Snake(2, 'test2', 100, [(4, 4), (4, 5), (4, 6), (4, 7)])
        board = Board(11, 11, [], [snake1, snake2])
        initial_state = GameState(1, 1, board, snake1)
        snake_moves = {1: 'up', 2:'right'}
        next_state = next_game_state(initial_state, snake_moves)
        self.assertNotIn(next_state.you, next_state.board.snakes)
        self.assertEqual(next_state.board.snakes[0].body,
                         [(5, 4), (4, 4), (4, 5), (4, 6)])
        # Original state must not be altered
        self.assertEqual(snake1.body, [(5, 5), (5, 6), (5, 7)])
        self.assertNotEqual(initial_state, next_state)

    def test_body_collision(self):
        snake1 = Snake(1, 'test1',  100, [(5, 5), (5, 6), (5, 7)])
        snake2 = Snake(2, 'test2', 100, [(4, 5), (4, 6), (4, 7)])
        board = Board(11, 11, [], [snake1, snake2])
        initial_state = GameState(1, 1, board, snake1)
        snake_moves = {1: 'up', 2:'right'}
        next_state = next_game_state(initial_state, snake_moves)
        self.assertEqual(next_state.board.snakes, [next_state.you])
        # Original state must not be altered
        self.assertEqual(snake1.body, [(5, 5), (5, 6), (5, 7)])
        self.assertNotEqual(initial_state, next_state)
