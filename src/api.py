import json
from bottle import HTTPResponse

ALLOWED_HEAD_TYPES = {'beluga', 'bendr', 'dead', 'evil', 'fang',
                       'pixel', 'regular', 'safe', 'sand-worm',
                       'shades', 'silly', 'smile', 'tongue'}
ALLOWED_TAIL_TYPES = {'block-bum', 'bolt', 'curled', 'fat-rattle',
                      'freckled', 'hook', 'pixel', 'regular',
                      'round-bum', 'sharp', 'slinky', 'small-rattle'}
ALLOWED_MOVEMENTS = {'up', 'down', 'left', 'right'}

def ping_response():
    return HTTPResponse(
        status=200
    )

def start_response(color, headType='regular', tailType='regular'):
    assert type(color) is str, \
        "Color value must be string"

    body = {}
    if color:
        body['color'] = color
    if headType:
        assert headType in ALLOWED_HEAD_TYPES,\
            'Head type must be in {}'.format(ALLOWED_HEAD_TYPES)
        body['headType'] = headType
    if tailType:
        assert tailType in ALLOWED_TAIL_TYPES,\
            'Tail type must be in {}'.format(ALLOWED_TAIL_TYPES)
        body['tailType'] = tailType

    return HTTPResponse(
        status=200,
        headers={
            "Content-Type": "application/json"
        },
        body=json.dumps(body)
    )

def move_response(move):
    assert move in ['up', 'down', 'left', 'right'], \
        "Move must be one of [up, down, left, right]"

    return HTTPResponse(
        status=200,
        headers={
            "Content-Type": "application/json"
        },
        body=json.dumps({
            "move": move
        })
    )

def end_response():
    return HTTPResponse(
        status=200
    )
