import numpy as np

class GameState:
    __slots__ = ['id', 'turn', 'board', 'you']

    def __init__(self, game_id, turn, board, you):
        self.id = game_id
        self.turn = turn
        self.board = board
        self.you = you

    @classmethod
    def from_dict(cls, d):
        game_id = d['game']['id']
        turn = d['turn']
        board = Board.from_dict(d['board'])
        you = Snake.from_dict(d['you'])
        return cls(game_id, turn, board, you)

    def to_dict(self):
        d = {}
        d['game'] = {'id':self.id}
        d['turn'] = self.turn
        d['board'] = self.board.to_dict()
        d['you'] = self.you.to_dict()
        return d

    def __eq__(self, other):
        return self.id == other.id and\
                self.turn == other.turn and\
                self.board == other.board and\
                self.you == other.you

class Board:

    def __init__(self, height, width, food, snakes):
        self.height = height
        self.width = width
        self.food = food
        self.snakes = snakes
        self.grid = np.zeros((width, height))
        for snake in snakes:
            for point in snake.body:
                self.grid[point] = 1

    @classmethod
    def from_dict(cls, d):
        snakes = [Snake.from_dict(s) for s in d['snakes']]
        food = [(p['x'], p['y']) for p in d['food']]
        return cls(d['height'], d['width'], food, snakes)

    def to_dict(self):
        d = {}
        d['height'] = self.height
        d['width'] = self.width
        d['food'] = [{'x':x, 'y':y} for x, y in self.food]
        d['snakes'] = [snake.to_dict() for snake in self.snakes]
        return d

    def __eq__(self, other):
        return self.height == other.height and\
                self.width == other.width and\
                self.food == other.food and\
                self.snakes == other.snakes

    def outside_grid(self, point):
        x, y = point
        return (x < 0 or x > self.width - 1) or (y < 0 or y > self.height -1)

    def get_open_adjacent_squares(self, point, whitelist=None):
        whitelist = whitelist if whitelist else []
        x, y = point
        squares = [(x+1, y), (x-1, y), (x, y+1), (x, y-1)]
        squares = [s for s in squares if not self.outside_grid(s) or s in whitelist]
        squares = [s for s in squares if self.grid[s] != 1 or s in whitelist]
        return squares

class Snake:
    __slots__ = ['id', 'name', 'health', 'body']

    def __init__(self, _id, name, health, body):
        self.id = _id
        self.name = name
        self.health = health
        self.body = body

    @classmethod
    def from_dict(cls, d):
        _id = d['id']
        name = d['name']
        health = d['health']
        body = [(p['x'], p['y']) for p in d['body']]
        return cls(_id, name, health, body)

    def to_dict(self):
        d = {}
        d['id'] = self.id
        d['name'] = self.name
        d['health'] = self.health
        d['body'] = [{'x':x, 'y':y} for x, y in self.body]
        return d

    def __str__(self):
        return 'Snake("{}", "{}", {}, {})'.format(
                self.id, self.name, self.health, self.body)

    def __repr__(self):
        return str(self)

    def __eq__(self, other):
        return self.id == other.id and\
                self.name == other.name and\
                self.health == other.health and\
                self.body == other.body
