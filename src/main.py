import json
import os
import bottle

from ai import SnakeAI
from api import ping_response, start_response, move_response, end_response
from common import getLogger
from gameobjects import GameState


weblog = getLogger('bottle')

game_instances = {} # {<gameid>: GameInstance}

class GameInstance:

    def __init__(self, game_id, initial_state, snake_ai):
        self.game_id = game_id
        self.history = [initial_state]
        self.snake_ai = snake_ai
        self.snake_ai.game_instance = self


@bottle.route('/')
def index():
    return '''
    Battlesnake documentation can be found at
       <a href="https://docs.battlesnake.io">https://docs.battlesnake.io</a>.
    '''

@bottle.post('/ping')
def ping():
    """
    A keep-alive endpoint used to prevent cloud application platforms,
    such as Heroku, from sleeping the application instance.
    """
    return ping_response()

@bottle.post('/start')
def start():
    data = bottle.request.json

    """
    TODO: If you intend to have a stateful snake AI,
            initialize your snake state here using the
            request's data if necessary.
    """
    weblog.debug(data)
    weblog.info('Starting new game')
    game_state = GameState.from_dict(data)
    game_instances[game_state.id] = GameInstance(game_state.id, game_state, SnakeAI())

    color = "#00FF00"

    return start_response(color)


@bottle.post('/move')
def move():
    data = bottle.request.json

    """
    TODO: Using the data from the endpoint request object, your
            snake AI must choose a direction to move in.
    """
    weblog.debug(data)
    game_state = GameState.from_dict(data)
    if game_state.id not in game_instances:
        game_instances[game_state.id] = GameInstance(game_state.id, game_state, SnakeAI())
    game_instance = game_instances[game_state.id]
    game_instance.history.append(game_state)
    ai = game_instance.snake_ai
    try:
        direction = ai(game_state)
    except KeyboardInterrupt:
        weblog.error("Killed by user, game_state: {}".format(data))
        raise KeyboardInterrupt()

    return move_response(direction)


@bottle.post('/end')
def end():
    data = bottle.request.json

    """
    TODO: If your snake AI was stateful,
        clean up any stateful objects here.
    """
    weblog.debug(data)
    weblog.info('Game ended')
    game_state = GameState.from_dict(data)

    return end_response()

# Expose WSGI app (so gunicorn can find it)
application = bottle.default_app()

if __name__ == '__main__':
    bottle.run(
        application,
        host=os.getenv('IP', '0.0.0.0'),
        port=os.getenv('PORT', '8080'),
        debug=os.getenv('DEBUG', False),
        quiet=True
    )
