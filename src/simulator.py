from gameobjects import Snake, Board, GameState
from common import direction_to_vector

def next_game_state(current_state, moves):
    # Move snakes, reduce health
    new_snakes = []
    for snake in current_state.board.snakes:
        snake_move = moves[snake.id]
        cur_x, cur_y = snake.body[0]
        dx, dy = direction_to_vector[snake_move]
        next_head = (cur_x + dx, cur_y + dy)
        new_body = [next_head] + snake.body[:]
        new_snake = Snake(snake.id, snake.name,
                          snake.health-1, new_body)
        new_snakes.append(new_snake)
        if snake.id == current_state.you.id:
            your_new_snake = new_snake

    # Handle food
    food_to_remove = []
    for snake in new_snakes:
        head = snake.body[0]
        tail = snake.body[-1]
        if head in current_state.board.food:
            snake.health = 100
            food_to_remove.append(head)
            snake.body.append(tail)
        else:
            # we appended a segment to the head end,
            # so remove the tail to make the snake move
            snake.body = snake.body[:-1]
    # Reset Food
    new_food = [food for food in current_state.board.food if food not in food_to_remove]
    #TODO: Add new food

    # Check for death
    dead_snakes = []
    for snake in new_snakes:
        # Starved
        head = snake.body[0]
        if snake.health <= 0:
            dead_snakes.append(snake)
            continue
        # out of bounds
        if current_state.board.outside_grid(head):
            dead_snakes.append(snake)
            continue
        # Collision
        for other in new_snakes:
            if other == snake:
                continue
            # Head collision
            other_head = other.body[0]
            if other_head == head and (len(other.body) > len(snake.body)):
                dead_snakes.append(snake)
                continue
            # body collision
            for segment in other.body[1:]:
                if segment == head:
                    dead_snakes.append(snake)
                    break
    new_snakes = [snake for snake in new_snakes if snake not in dead_snakes]

    new_board = Board(current_state.board.height,
                      current_state.board.width,
                      new_food,
                      new_snakes)

    next_game_state = GameState(current_state.id,
                                current_state.turn + 1,
                                new_board, your_new_snake)
    return next_game_state

